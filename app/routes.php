<?php

declare(strict_types=1);

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use \App\Application\Actions\Index;
use \App\Application\Actions\User;
use \App\Application\Actions\Champion;
use \App\Application\Actions\Item;
use \App\Application\Actions\Spell;
use \App\Application\Actions\Report;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });


    $app->group('/api', function (Group $group) {
        $group->get('', Index\HomeAction::class);
        $group->get('/version', Index\VersionAction::class);
        $group->get('/versions', Index\ListVersionAction::class);
        $group->get('/languages', Index\LanguageAction::class);
        $group->get('/champions/{version}/{region}', Champion\ListChampionAction::class);
        $group->get('/champion/{version}/{region}/{champion}', Champion\ChampionDetailAction::class);
        $group->get('/items/{version}/{region}', Item\ListItemAction::class);
        $group->get('/spells/{version}/{region}', Spell\ListSpellAction::class);
        $group->post('/report', Report\ReportAction::class);
    });

    $app->group('/users', function (Group $group) {
        $group->get('', User\ListUsersAction::class);
        $group->get('/{id}', User\ViewUserAction::class);
    });
};