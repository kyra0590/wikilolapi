<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoryChampion extends Model
{
    protected $table = 'story_champion';


    protected $fillable = [
        'id', 'name', 'language', 'story'
    ];

    // protected $dispatchesEvents = [
    //     'deleting' => PincodeDeleting::class
    // ];
}