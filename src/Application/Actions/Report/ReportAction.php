<?php

namespace App\Application\Actions\Report;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Application\Configs\Helper;
use App\Application\Actions\LogAction;
use App\Models\Report;


class ReportAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action( ) :Response
    {  
        $method = $this->request->getMethod();
        $params = $this->request->getParsedBody();
        $response = [];
        if($method == 'POST') {
            if(!empty($params['name']) &&
            !empty($params['email']) &&
            !empty($params['issue']) && 
            !empty($params['comment'])) {
                $response = Report::create([
                    'name' => $params['name'],
                    'email' => $params['email'],
                    'issue' => $params['issue'],
                    'comment' => $params['comment']
                ])->toArray(); 
                $this->logger->info("Report : ". var_export($params,true));
                return $this->respondWithData($response);
            }
        }
        $response['error'] = 'Incorrect Parameters';
        $this->logger->info("Report : Incorrect Parameters");
        return $this->respondWithData($response, 400);
    }
}
