<?php

namespace App\Application\Actions\Champion;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Application\Configs\Helper;
use App\Application\Configs\Constants;
use App\Application\Actions\LogAction;
use App\Models\StoryChampion;

class ListChampionAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action() :Response
    {   
        $params  = $this->args;
        $queryParams = $this->request->getQueryParams();
        $champions = [];
        if(!empty($params['version']) && !empty($params['region'])) {
            $versions = Helper::getVersions();
            $languages = Helper::getLanguages();
            if( in_array($params['version'], $versions) && in_array($params['region'], $languages)) {
                $region = $params['region'];
                $champions = Helper::getDataByUrl("champions", $params);
                $champions = $champions->data;

                if(!empty($queryParams['story'])) {
                    self::getStoryChampion($champions, strtolower($params['region']));
                }
            }
        }

        $this->logger->info("List Champions : ". var_export($champions,true));
        
        return $this->respondWithData(
             $champions
        );

    }

    function getStoryChampion($champions, $region){

        foreach($champions as $key => $champion) {
            $championName = strtolower($key);
            $url = Constants::LOL_STORY_CHAMPION;
            $url = str_replace('{region}', $region, $url);
            switch ($championName) {
                case 'renata':
                    $newNameChampion = strtolower(str_replace(' ', '',$champion->name));
                    $url = str_replace('{champion}', $newNameChampion, $url);
                    break;
                default:
                    $url = str_replace('{champion}', $championName, $url);
                    break;
            }
            $champion = StoryChampion::where('name', '=', $championName)
                    ->where('language', '=', $region)
                    ->first();
            if(empty($champion)) {
                $client = new \GuzzleHttp\Client(['verify' => false ]); 
                $response = $client->request('GET', $url); 
                $story = $response->getBody(); 
                $newChamp = StoryChampion::create([
                    'name' => $championName,
                    'language' => $region,
                    'story' => $story
                ]); 
            } else {
                if(empty($champion->story)) {
                    $client = new \GuzzleHttp\Client(['verify' => false ]); 
                    $response = $client->request('GET', $url); 
                    $story = $response->getBody(); 
                    $champion->story = $story;
                    $champion->save();
                }
            }
        }
    }
}
