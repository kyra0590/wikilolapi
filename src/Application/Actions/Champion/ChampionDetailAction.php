<?php

namespace App\Application\Actions\Champion;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Application\Configs\Helper;
use App\Application\Configs\Helper_Champion;
use App\Application\Actions\LogAction;
use App\Models\StoryChampion;

class ChampionDetailAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action( ) :Response
    {   
        $params  = $this->args;
        $championDetail = null;
        if(!empty($params['version']) && !empty($params['region']) && !empty($params['champion'])) {
            $versions = Helper::getVersions();
            $languages = Helper::getLanguages();
            if( in_array($params['version'], $versions) && in_array($params['region'], $languages)) {
                $region = $params['region'];
                $champion = str_replace(' ', '', $params['champion']);
                $championDetail = Helper::getDataByUrl("champion", $params);
                $championDetail =  $championDetail->data->{$params['champion']};
                $moreDetail = self::getStoryChampion($champion, $region);
                $championDetail->moreDetail = $moreDetail; 
                                
            }
        }
        $this->logger->info("Champion Details : ". var_export($championDetail,true));
        return $this->respondWithData( $championDetail );
    }

    function getStoryChampion($championName, $region) {
        $moreDetail = [];
        $championStory = StoryChampion::where('language' ,'=' ,strtolower($region))
        ->where('name' ,'=' , strtolower($championName) )->first();
        if($championStory) {
            $story = json_decode($championStory->story, true);
            $champion = $story['champion'];
            $moreDetail['role'] = $champion['role'];
            $moreDetail['biography'] = [
                'full' => $champion['biography']['full'],
                'short' => $champion['biography']['short'],
            ];
            $moreDetail['related-champions'] = $story['related-champions'];
            $modules = [];
            if( $story['modules']) {
                foreach($story['modules'] as $key => $value){
                    $type = $value['type'];
                    $modules[$type][] =  $value;
                }
            }
            $moreDetail['modules'] = $modules;
        }
        return $moreDetail;
    }
}
