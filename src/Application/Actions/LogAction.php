<?php

namespace App\Application\Actions;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;

abstract class LogAction extends Action
{
    /**
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        parent::__construct($logger);
    }
}