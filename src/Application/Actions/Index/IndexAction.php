<?php
declare(strict_types=1);

namespace App\Application\Actions\Index;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Configs\Helper;
use App\Application\Actions\LogAction;

class IndexAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {   
        // try languages
        $languages = Helper::getLanguages();
        $this->logger->info("Index Language : ". var_export($languages));
        return $this->respondWithData([
            'index' => $languages
        ]);

    }
}
