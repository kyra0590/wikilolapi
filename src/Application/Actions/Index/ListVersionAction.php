<?php
declare(strict_types=1);

namespace App\Application\Actions\Index;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Configs\Constants;
use App\Application\Configs\Helper;
use App\Application\Actions\LogAction;

class ListVersionAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {   
        $versions = Helper::getVersions();
        $this->logger->info("List versions : " . var_export($versions,true));
        return $this->respondWithData(
            $versions
        );
    }
}
