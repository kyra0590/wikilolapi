<?php
declare(strict_types=1);

namespace App\Application\Actions\Index;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Configs\Helper;
use App\Application\Actions\LogAction;

class LanguageAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {   
        // try languages
        $languages = Helper::getLanguages();
        $this->logger->info("List Language : ". var_export($languages,true));
        return $this->respondWithData([
            'languages' => $languages
        ]);

    }
}
