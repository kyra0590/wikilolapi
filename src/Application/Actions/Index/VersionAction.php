<?php

declare(strict_types=1);

namespace App\Application\Actions\Index;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Configs\Constants;
use App\Application\Configs\Helper;
use App\Application\Actions\LogAction;

class VersionAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $version = Helper::getLastVersion();
        $this->logger->info("Last version : {$version}.");
        return $this->respondWithData([
            'version' => $version
        ]);
    }
}