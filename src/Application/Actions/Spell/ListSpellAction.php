<?php

namespace App\Application\Actions\Spell;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Application\Configs\Helper;
use App\Application\Actions\LogAction;

class ListSpellAction extends LogAction
{
    /**
     * {@inheritdoc}
     */
    protected function action( ) :Response
    {   
        $params  = $this->args;
        $items = [];
        if(!empty($params['version']) && !empty($params['region'])) {
            $versions = Helper::getVersions();
            $languages = Helper::getLanguages();
            if( in_array($params['version'], $versions) && in_array($params['region'], $languages)) {
                $items = Helper::getDataByUrl("spells", $params);
                $items = $items->data;
            }
        }
        $this->logger->info("List items : ". var_export($items,true));
        return $this->respondWithData($items);

    }
}
