<?php

namespace App\Application\Configs;

use App\Application\Configs\Constants;

class Helper
{

    private static function loadContent($uri)
    {
        return json_decode(file_get_contents($uri));
    }

    private static function replaceURL($url, $replaces)
    {
        if (!empty($replaces)) {
            foreach ($replaces as $key => $value) {
                $url = str_replace("{" . $key . "}", $value,  $url);
            }
        }
        return $url;
    }

    public static function getVersions()
    {
        $versions = self::loadContent(Constants::LOL_VERSIONS);
        return $versions;
    }


    public static function getLastVersion()
    {
        $versions = self::getVersions();
        $lastVersion = "";
        if (count($versions) > 0) {
            $lastVersion = reset($versions);
        }
        return $lastVersion;
    }

    public static function getLanguages()
    {
        $languages = self::loadContent(Constants::LOL_LANGUAGES);
        return $languages;
    }

    public static function getDataByUrl($type, $data)
    {
        $url = "";
        switch ($type) {
            case 'champion':
                $url = Constants::LOL_CHAMPION;
                break;
            case 'items':
                $url = Constants::LOL_ITEMS;
                break;
            case 'spells':
                $url = Constants::LOL_SPELLS;
                break;
            case "champions":
                $url = Constants::LOL_CHAMPIONS;
                break;
        }
        if (empty($url)) return null;
        $replaceUrl = self::replaceURL($url, $data);
        $items = self::loadContent($replaceUrl);
        return $items;
    }
}