<?php

namespace App\Application\Configs;

class Constants
{
    public const LOL_HOST = 'https://ddragon.leagueoflegends.com';
    public const LOL_API =  'https://ddragon.leagueoflegends.com/api';
    public const LOL_CDN =  'https://ddragon.leagueoflegends.com/cdn';
    public const LOL_LANGUAGES = 'https://ddragon.leagueoflegends.com/cdn/languages.json';
    public const LOL_VERSIONS = 'https://ddragon.leagueoflegends.com/api/versions.json';
    public const LOL_CHAMPIONS = 'https://ddragon.leagueoflegends.com/cdn/{version}/data/{region}/champion.json';
    public const LOL_CHAMPION = 'https://ddragon.leagueoflegends.com/cdn/{version}/data/{region}/champion/{champion}.json';
    public const LOL_ITEMS = 'https://ddragon.leagueoflegends.com/cdn/{version}/data/{region}/item.json';
    public const LOL_ITEM_IMAGE = 'https://ddragon.leagueoflegends.com/cdn/{version}/img/item/{image}';
    public const LOL_SPELLS = 'https://ddragon.leagueoflegends.com/cdn/{version}/data/{region}/summoner.json';
    public const LOL_SPELL_IMAGE = 'https://ddragon.leagueoflegends.com/cdn/{version}/img/spell/{image}';
    public const LOL_MINIMAP_IMAGE = 'https://ddragon.leagueoflegends.com/cdn/6.8.1/img/map/map11.png';
    public const LOL_STORY_CHAMPION = 'https://universe-meeps.leagueoflegends.com/v1/{region}/champions/{champion}/index.json';
}